# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aljoša Žagar <anthon.manix@gmail.com>, 2017
# Arnold Marko <arnold.marko@gmail.com>, 2019-2020
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-23 00:31+0200\n"
"PO-Revision-Date: 2020-10-29 12:36+0000\n"
"Last-Translator: Arnold Marko <arnold.marko@gmail.com>\n"
"Language-Team: Slovenian (http://www.transifex.com/xfce/xfce-apps/language/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: ../data/interfaces/parole.ui.h:1
#: ../data/desktop/org.xfce.Parole.desktop.in.in.h:1
#: ../src/parole-player.c:591 ../src/parole-player.c:1437
#: ../src/parole-player.c:1788 ../src/parole-about.c:68
#: ../src/plugins/mpris2/mpris2-provider.c:189
msgid "Parole Media Player"
msgstr "Parole medijski predvajalnik"

#: ../data/interfaces/parole.ui.h:2
msgid "_Media"
msgstr "_Medij"

#: ../data/interfaces/parole.ui.h:3
msgid "_Open..."
msgstr "_Odpri..."

#: ../data/interfaces/parole.ui.h:4
msgid "Open _Location..."
msgstr "Odpri _lokacijo..."

#: ../data/interfaces/parole.ui.h:5
msgid "Open _Recent"
msgstr "Odpri _nedavno"

#: ../data/interfaces/parole.ui.h:6
msgid "_Save Playlist..."
msgstr "_Shrani seznam predvajanja..."

#: ../data/interfaces/parole.ui.h:7 ../src/parole-disc.c:105
#: ../src/parole-disc.c:140
msgid "Insert Disc"
msgstr "Vstavi disk"

#. * Exit
#: ../data/interfaces/parole.ui.h:8 ../src/plugins/tray/tray-provider.c:168
msgid "_Quit"
msgstr "_Končaj"

#: ../data/interfaces/parole.ui.h:9
msgid "_Playback"
msgstr "_Predvajanje"

#: ../data/interfaces/parole.ui.h:10
msgid "_Repeat"
msgstr "_Ponovi"

#: ../data/interfaces/parole.ui.h:11
msgid "_Shuffle"
msgstr "_Premešaj"

#. Create dialog
#: ../data/interfaces/parole.ui.h:12 ../src/parole-player.c:2808
msgid "Go to position"
msgstr "Pojdi na položaj"

#: ../data/interfaces/parole.ui.h:13
msgid "_DVD"
msgstr "_DVD"

#: ../data/interfaces/parole.ui.h:14
msgid "_DVD Menu"
msgstr "_DVD meni"

#: ../data/interfaces/parole.ui.h:15
msgid "_Title Menu"
msgstr "_Naslovni meni"

#: ../data/interfaces/parole.ui.h:16
msgid "_Audio Menu"
msgstr "_Zvočni meni"

#: ../data/interfaces/parole.ui.h:17
msgid "A_ngle Menu"
msgstr "Me_ni kotov"

#: ../data/interfaces/parole.ui.h:18
msgid "_Chapters"
msgstr "_Poglavja"

#: ../data/interfaces/parole.ui.h:19
msgid "_Chapter Menu"
msgstr "Meni _poglavji"

#: ../data/interfaces/parole.ui.h:20
msgid "_Audio"
msgstr "_Zvok"

#: ../data/interfaces/parole.ui.h:21
msgid "_Audio Track"
msgstr "_Zvočni zapis"

#: ../data/interfaces/parole.ui.h:22 ../src/parole-player.c:797
msgid "Empty"
msgstr "Prazno"

#: ../data/interfaces/parole.ui.h:23
msgid "Volume _Up"
msgstr "Glasnost _gor"

#: ../data/interfaces/parole.ui.h:24
msgid "Volume _Down"
msgstr "Glasnost _dol"

#: ../data/interfaces/parole.ui.h:25
msgid "_Mute"
msgstr "_Nemo"

#: ../data/interfaces/parole.ui.h:26
msgid "_Video"
msgstr "_Video"

#: ../data/interfaces/parole.ui.h:27 ../src/parole-player.c:1981
#: ../src/parole-player.c:2134
msgid "_Fullscreen"
msgstr "_Celozaslonski način"

#: ../data/interfaces/parole.ui.h:28
msgid "_Aspect Ratio"
msgstr "R_azmerje slike"

#: ../data/interfaces/parole.ui.h:29 ../src/parole-player.c:728
msgid "None"
msgstr "Noben"

#: ../data/interfaces/parole.ui.h:30
msgid "Auto"
msgstr "Samodejno"

#: ../data/interfaces/parole.ui.h:31
msgid "Square"
msgstr "Kvadrat"

#: ../data/interfaces/parole.ui.h:32
msgid "4:3 (TV)"
msgstr "4:3 (TV)"

#: ../data/interfaces/parole.ui.h:33
msgid "16:9 (Widescreen)"
msgstr "16:9 (Širok zaslon)"

#: ../data/interfaces/parole.ui.h:34
msgid "20:9 (DVB)"
msgstr "20:9 (DVB)"

#: ../data/interfaces/parole.ui.h:35
msgid "_Subtitles"
msgstr "_Podnapisi"

#: ../data/interfaces/parole.ui.h:36
msgid "Select Text Subtitles..."
msgstr "Izberite podnapise..."

#: ../data/interfaces/parole.ui.h:37
msgid "_Tools"
msgstr "_Orodja"

#: ../data/interfaces/parole.ui.h:38
msgid "P_lugins"
msgstr "V_tičniki"

#: ../data/interfaces/parole.ui.h:39
msgid "_Preferences"
msgstr "_Možnosti"

#: ../data/interfaces/parole.ui.h:40
msgid "_Help"
msgstr "_Pomoč"

#: ../data/interfaces/parole.ui.h:41
msgid "_Report a Bug..."
msgstr "P_rijavite hrošča..."

#: ../data/interfaces/parole.ui.h:42
msgid "_Keyboard Shortcuts"
msgstr "Bljižnice na tip_kovnici"

#: ../data/interfaces/parole.ui.h:43
msgid "Display Parole user manual"
msgstr "Prikaži Parole uporabniški priročnik"

#: ../data/interfaces/parole.ui.h:44
msgid "_Contents"
msgstr "_Vsebina"

#: ../data/interfaces/parole.ui.h:45
msgid "_About"
msgstr "_O programu"

#: ../data/interfaces/parole.ui.h:46
msgid "<span color='#F4F4F4'><b><big>Unknown Song</big></b></span>"
msgstr "<span color='#F4F4F4'><b><big>neznana skladba</big></b></span>"

#: ../data/interfaces/parole.ui.h:47
msgid ""
"<big><span color='#BBBBBB'><i>on</i></span> <span color='#F4F4F4'>Unknown "
"Album</span></big>"
msgstr "<big><span color='#BBBBBB'><i>na</i></span> <span color='#F4F4F4'>neznanem albumu</span></big>"

#: ../data/interfaces/parole.ui.h:48
msgid ""
"<big><span color='#BBBBBB'><i>by</i></span> <span color='#F4F4F4'>Unknown "
"Artist</span></big>"
msgstr "<big><span color='#BBBBBB'><i>od</i></span> <span color='#F4F4F4'>neznanega izvajalca</span></big>"

#: ../data/interfaces/parole.ui.h:49
msgid "0:00"
msgstr "0:00"

#: ../data/interfaces/parole.ui.h:51
#, no-c-format
msgid "Buffering (0%)"
msgstr "Polnjenje medpomnilnika (0%)"

#: ../data/interfaces/parole.ui.h:52
msgid "/"
msgstr "/"

#: ../data/interfaces/mediachooser.ui.h:1
msgid "Open Media Files"
msgstr "Odpri medijske datoteke"

#: ../data/interfaces/playlist.ui.h:1
#: ../data/interfaces/parole-settings.ui.h:28
#: ../data/interfaces/shortcuts.ui.h:13 ../src/parole-medialist.c:742
#: ../src/parole-medialist.c:781 ../src/parole-player.c:3188
msgid "Playlist"
msgstr "Seznam predvajanja"

#: ../data/interfaces/playlist.ui.h:2
msgid "Disc Playlist"
msgstr "Seznam predvajanja za disk"

#: ../data/interfaces/playlist.ui.h:3
msgid "Add to playlist..."
msgstr "Dodaj na seznam predvajanja..."

#: ../data/interfaces/playlist.ui.h:4
msgid "Add"
msgstr "Dodaj"

#: ../data/interfaces/playlist.ui.h:5
msgid "Remove selected media"
msgstr "Odstrani izbrane medije"

#: ../data/interfaces/playlist.ui.h:6
msgid "Remove"
msgstr "Odstrani"

#: ../data/interfaces/playlist.ui.h:7
msgid "Clear playlist"
msgstr "Počisti seznam predvajanja"

#: ../data/interfaces/playlist.ui.h:8
msgid "Clear"
msgstr "Počisti"

#: ../data/interfaces/playlist.ui.h:9
msgid "Repeat"
msgstr "Ponavljaj"

#: ../data/interfaces/playlist.ui.h:10
msgid "Shuffle"
msgstr "Premešaj"

#: ../data/interfaces/playlist.ui.h:11
msgid "Move Up"
msgstr "Premakni gor"

#: ../data/interfaces/playlist.ui.h:12
msgid "Move Down"
msgstr "Premakni dol"

#: ../data/interfaces/playlist.ui.h:13
msgid "Replace playlist when opening files"
msgstr "Zamenjaj seznam predvajanja od odpiranju datotek"

#: ../data/interfaces/playlist.ui.h:14
msgid "Play opened files"
msgstr "Predvajaj odprte datoteke"

#: ../data/interfaces/playlist.ui.h:15
#: ../data/interfaces/parole-settings.ui.h:26
msgid "Remember playlist"
msgstr "Zapomni si seznam predvajanja"

#: ../data/interfaces/plugins.ui.h:1
msgid "Parole Plugins"
msgstr "Parloe vtičniki"

#: ../data/interfaces/plugins.ui.h:2
msgid "Extend your media player"
msgstr "Razširi medijski predvajalnik"

#: ../data/interfaces/plugins.ui.h:3
msgid "Plugin"
msgstr "Vtičnik"

#: ../data/interfaces/plugins.ui.h:4
msgid "<b>Description</b>"
msgstr "<b>Opis</b>"

#: ../data/interfaces/plugins.ui.h:5
msgid "<b>Author</b>"
msgstr "<b>Avtor</b>"

#: ../data/interfaces/plugins.ui.h:6
msgid "Visit Website"
msgstr "Obišči spletno stran"

#: ../data/interfaces/parole-settings.ui.h:1
msgid "Automatic"
msgstr "Samodejno"

#: ../data/interfaces/parole-settings.ui.h:2
msgid "X Window System (X11/XShm/Xv)"
msgstr "X Window System (X11/XShm/Xv)"

#: ../data/interfaces/parole-settings.ui.h:3
msgid "X Window System (No Xv)"
msgstr "X Window System (No Xv)"

#: ../data/interfaces/parole-settings.ui.h:4
msgid "Parole Settings"
msgstr "parole nastavitve"

#: ../data/interfaces/parole-settings.ui.h:5
msgid "Configure your media player"
msgstr "Konfiguriranje medijskega predvajalnika"

#: ../data/interfaces/parole-settings.ui.h:6
msgid "Disable screensaver when playing movies"
msgstr "Izklopi ohranjevalnik zaslona med predvajanjem videa"

#: ../data/interfaces/parole-settings.ui.h:7
msgid "<b>Screensaver</b>"
msgstr "<b>Ohranjevalnik zaslona</b>"

#: ../data/interfaces/parole-settings.ui.h:8
msgid "Show visual effects when an audio file is played"
msgstr "Prikaži vizualne učnike med predvajanjem zvoka"

#: ../data/interfaces/parole-settings.ui.h:9
msgid "Visualization type:"
msgstr "Vrsta vizualizacije:"

#: ../data/interfaces/parole-settings.ui.h:10
msgid "<b>Audio Visualization</b>"
msgstr "<b>Vizualizacija zvoka</b>"

#: ../data/interfaces/parole-settings.ui.h:11
msgid "Enable keyboard multimedia keys"
msgstr "Vklopi multimedijske tipke na tipkovnici"

#: ../data/interfaces/parole-settings.ui.h:12
msgid "<b>Keyboard</b>"
msgstr "<b>Tipkovnica</b>"

#: ../data/interfaces/parole-settings.ui.h:13
#: ../data/interfaces/shortcuts.ui.h:1
msgid "General"
msgstr "Splošno"

#: ../data/interfaces/parole-settings.ui.h:14
msgid "Please restart Parole for this change to take effect."
msgstr "Ponovno zaženite Parole za uveljavitev teh sprememb."

#: ../data/interfaces/parole-settings.ui.h:15
msgid "<b>Video Output</b>"
msgstr "<b>Video izhod</b>"

#: ../data/interfaces/parole-settings.ui.h:16
msgid "Brightness:"
msgstr "Svetlost:"

#: ../data/interfaces/parole-settings.ui.h:17
msgid "Contrast:"
msgstr "Kontrast:"

#: ../data/interfaces/parole-settings.ui.h:18
msgid "Hue:"
msgstr "Odtenek (hue):"

#: ../data/interfaces/parole-settings.ui.h:19
msgid "Saturation:"
msgstr "Nasičenost:"

#: ../data/interfaces/parole-settings.ui.h:20
msgid "Reset to defaults"
msgstr "Ponastavi privzeto"

#: ../data/interfaces/parole-settings.ui.h:21
msgid "<b>Color Balance</b>"
msgstr "<b>Ravnovesje barv</b>"

#: ../data/interfaces/parole-settings.ui.h:22
msgid "Display"
msgstr "Prikaz"

#: ../data/interfaces/parole-settings.ui.h:23
msgid "Always replace playlist with opened files"
msgstr "vedno zamenjaj seznam predvajanja z odprtimi datotekami"

#: ../data/interfaces/parole-settings.ui.h:24
msgid "Check and remove duplicate media entries"
msgstr "Preveri in odstrani podvojene vnose medijev"

#: ../data/interfaces/parole-settings.ui.h:25
msgid "Start playing opened files"
msgstr "Začni predvajati odprte datoteke"

#: ../data/interfaces/parole-settings.ui.h:27
msgid "<b>Playlist Settings</b>"
msgstr "<b>Nastavitve seznama predvajanja</b>"

#: ../data/interfaces/parole-settings.ui.h:29
msgid "Automatically show subtitles when playing movie file"
msgstr "Samojedno prikaži podnapise pri predvajanju filmov"

#: ../data/interfaces/parole-settings.ui.h:30
msgid "Font:"
msgstr "Pisava:"

#: ../data/interfaces/parole-settings.ui.h:31
msgid "Encoding:"
msgstr "Kodiranje:"

#: ../data/interfaces/parole-settings.ui.h:32
msgid "<b>Subtitle Settings</b>"
msgstr "</b>Nastavitve podnapisov</b>"

#: ../data/interfaces/parole-settings.ui.h:33
msgid "Subtitles"
msgstr "Podnapisi"

#: ../data/interfaces/save-playlist.ui.h:1
msgid "By Extension"
msgstr "po končnici"

#: ../data/interfaces/save-playlist.ui.h:2
msgid "Save Playlist as..."
msgstr "Shrani seznam predvajanja kot"

#: ../data/interfaces/save-playlist.ui.h:3
msgid "File Type"
msgstr "Vrsta datoteke"

#: ../data/interfaces/save-playlist.ui.h:4
msgid "Extension"
msgstr "Končnica"

#: ../data/interfaces/save-playlist.ui.h:5
msgid "Select File Types (By Extension)"
msgstr "Izbira vrst datotek (po končici)"

#: ../data/interfaces/open-location.ui.h:1
msgid "Open Network Location"
msgstr "Odpri omrežno mesto"

#: ../data/interfaces/open-location.ui.h:2 ../src/parole-open-location.c:181
msgid "Clear History"
msgstr "Počisti zgodovino"

#: ../data/interfaces/open-location.ui.h:3
msgid "<big><b>Open Network Location</b></big>"
msgstr "<big><b>Lokacija odprtega omrežja</b></big>"

#: ../data/interfaces/shortcuts.ui.h:2
msgid "Open file"
msgstr "Odpri datoteko"

#: ../data/interfaces/shortcuts.ui.h:3
msgid "Open location"
msgstr "Odpri lokacijo"

#: ../data/interfaces/shortcuts.ui.h:4 ../src/plugins/tray/tray-provider.c:325
msgid "Quit"
msgstr "Končaj"

#: ../data/interfaces/shortcuts.ui.h:5
msgid "View"
msgstr "Pogled"

#: ../data/interfaces/shortcuts.ui.h:6
msgid "Toggle fullscreen"
msgstr "Cel zaslon"

#: ../data/interfaces/shortcuts.ui.h:7
msgid "Exit fullscreen"
msgstr "Uokvirjen zaslon"

#: ../data/interfaces/shortcuts.ui.h:8
msgid "Toggle menubar"
msgstr "Preklopi menu"

#: ../data/interfaces/shortcuts.ui.h:9
msgid "Volume"
msgstr "Glasnost"

#: ../data/interfaces/shortcuts.ui.h:10
msgid "Increase volume"
msgstr "Povečaj glasnost"

#: ../data/interfaces/shortcuts.ui.h:11
msgid "Decrease volume"
msgstr "Zmanjšaj glasnost"

#: ../data/interfaces/shortcuts.ui.h:12 ../src/parole-player.c:2382
msgid "Mute"
msgstr "Nemo"

#: ../data/interfaces/shortcuts.ui.h:14
msgid "Toggle playlist"
msgstr "Preklopi seznam predvajanja"

#: ../data/interfaces/shortcuts.ui.h:15
msgid "Select previous item"
msgstr "Izberi prejšnji predmet"

#: ../data/interfaces/shortcuts.ui.h:16
msgid "Select next item"
msgstr "Izberi naslednji predmet"

#: ../data/interfaces/shortcuts.ui.h:17
msgid "Remove selected item"
msgstr "Odstrani izbrani predmet"

#: ../data/interfaces/shortcuts.ui.h:18
msgid "Playback"
msgstr "Predvajanje"

#: ../data/interfaces/shortcuts.ui.h:19
#: ../data/desktop/org.xfce.Parole.desktop.in.in.h:3
msgid "Play/Pause"
msgstr "Predvajaj/Pavza"

#: ../data/interfaces/shortcuts.ui.h:20 ../src/main.c:241
msgid "Previous track"
msgstr "Prejšnji zapis"

#: ../data/interfaces/shortcuts.ui.h:21 ../src/main.c:239
msgid "Next track"
msgstr "Naslednji zapis"

#: ../data/interfaces/shortcuts.ui.h:22
msgid "Go back 10 seconds"
msgstr "Nazaj za 10 sekund"

#: ../data/interfaces/shortcuts.ui.h:23
msgid "Go forward 10 seconds"
msgstr "Naprej za 10 sekund"

#: ../data/interfaces/shortcuts.ui.h:24
msgid "Go back 1 minute"
msgstr "Nazaj za minuto"

#: ../data/interfaces/shortcuts.ui.h:25
msgid "Go forward 1 minute"
msgstr "Naprej za minuto"

#: ../data/interfaces/shortcuts.ui.h:26
msgid "Go back 10 minutes"
msgstr "Nazaj za 10 minut"

#: ../data/interfaces/shortcuts.ui.h:27
msgid "Go forward 10 minutes"
msgstr "Naprej za 10 minut"

#: ../data/desktop/org.xfce.Parole.desktop.in.in.h:2
msgid "Play your media"
msgstr "Predvajaj svoje medije"

#: ../data/desktop/org.xfce.Parole.desktop.in.in.h:4
#: ../src/parole-player.c:3337 ../src/plugins/notify/notify-provider.c:190
msgid "Previous Track"
msgstr "Prejšnji zapis"

#: ../data/desktop/org.xfce.Parole.desktop.in.in.h:5
#: ../src/parole-player.c:3355 ../src/plugins/notify/notify-provider.c:202
msgid "Next Track"
msgstr "Naslednji zapis"

#: ../data/metainfo/parole.appdata.xml.in.h:1
msgid ""
"Parole is a modern simple media player based on the GStreamer framework and "
"written to fit well in the Xfce desktop. It is designed with simplicity, "
"speed and resource usage in mind."
msgstr "Parole je moderen in preprost predvajalnik, ki temelji na GStreamer okvirju in je napisan, da se dobro zlije z Xfce namizjem. Načrtovan je z mislijo na enostavnost, hitrost in varčevanje z viri."

#: ../data/metainfo/parole.appdata.xml.in.h:2
msgid ""
"Parole features playback of local media files, including video with "
"subtitles support, Audio CDs, DVDs, and live streams. Parole is also "
"extensible via plugins."
msgstr "Parole omogoča predvajanje lokalnih medijskih datotek, videov s podnapisi, zvočnih zgoščenk, DVDjev in živih tokov. Parole je mogoče tudi nadgraditi s pomočjo vtičnikov."

#: ../data/metainfo/parole.appdata.xml.in.h:3
msgid "This is a minor translations-only release."
msgstr "To je manjši popravek prevodov."

#: ../data/metainfo/parole.appdata.xml.in.h:4
msgid ""
"This release marks the first stable release of the 1.0 series. A new "
"Automatic video output option was added to improve usage on virtual machines"
" and low power devices. A large number of bugs were also resolved, making "
"this release suitable for all users."
msgstr "Ta izdaja je prva stabilna 1.0 različica. Dodana je opcija za samodejni video izhod, za boljšo izrabo slabših računalnikov in virtualnih naprav. Odpravljno je veliko število hroščev, zaradi česar je izdaja primerna za vse uproabnike."

#: ../data/metainfo/parole.appdata.xml.in.h:5
msgid ""
"This development release is a large cleanup of the codebase, making Parole "
"lighter and easier to improve. All URLs have been updated to HTTPS. A large "
"number of bugs have been resolved and a new keyboard shortcuts helper has "
"been introduced."
msgstr "Ta razvojna izdaja je doživela očiščenje kode, kar ima za posledico, da je program manjši in lažji za posodobaljanje. Posodobljeni so bili vsi URLji na HTTPS. Odpravljeni so bili številni hrošči, vpeljan pa je bil nov pomočnik za rabo tipkovnice."

#: ../data/metainfo/parole.appdata.xml.in.h:6
msgid ""
"This development release fixes several bugs, typos, and historical details "
"present in the documentation. Tags are now processed on files delivered over"
" HTTP/S. The clutter video backend is disabled for GTK+ 3.22 and newer due "
"to significant API changes."
msgstr "V teh dlovni izdaji so odpravljeni številni hrošči, tipkarske napake in podrobnosti v dokumentaciji. Oznake so na novo obdelane na datotekah, ki so dostavljene preko HTTP/S. Clutter podpora za GTK+3.22 in novejše je odpravljena zaradi sprememb v API."

#: ../data/metainfo/parole.appdata.xml.in.h:7
msgid ""
"This development release adds a new mini mode and the ability to play or "
"replay content by clicking the circle icons. Theming is simplified and "
"filenames are displayed when no ID3 tags are found."
msgstr "Ta razvojna izdaja dodaja novi mini način, ki omogoča predvajanje preko okroglih ikon. Poenostavljeno je tematiziranje, imena datotek so prikazana, ko niso najdene ID3 oznake."

#: ../data/metainfo/parole.appdata.xml.in.h:8
msgid ""
"This release improves the build process and includes several bug fixes. This"
" new stable release is a recommended upgrade for all users."
msgstr "Ta izdaja poenosatvi proces gradnje in vsebuje številne popravke hroščev. Je priporočena za sve uporabnike."

#: ../data/metainfo/parole.appdata.xml.in.h:9
msgid ""
"This unstable development release adds the new clutter backend and cleans up"
" deprecated Gtk3 symbols. It also adds a \"go to position\" feature and "
"several bugs have been addressed."
msgstr "Ta nestabilna izdaja dodaj novi clutter in očisti zastarele Gtk3 simbole. Dodaj tudi funkcijo \"pojdi na\" ter odpravlja hrošče."

#: ../data/metainfo/parole.appdata.xml.in.h:10
msgid "This release fixes a problem with detecting DVD drive locations."
msgstr "Ta izdaja odpravlja težavo z odkrivanjem lokacije DVDja"

#: ../data/metainfo/parole.appdata.xml.in.h:11
msgid ""
"This release features the completed transition to the Gtk+3 toolkit, "
"improvements in the user-interface (more streamlined menus), better "
"playlists. Furthermore existing plugins have been improved and a new one has"
" been added (MPRIS2). Most of the artwork has been updated for this release "
"and we use the GStreamer1.0 framework now by default."
msgstr "Ta izdaja zaključuje prehod na Gtk+3. Ima izboljšan vmesnik (boljši meiji) in bolješ delo s seznami predvajanja. Obstoječi vtičniki so bili izboljšani (MPRIS2). Grafika je bila posodobljena, GStreamer 1.0 pa je sedaj standard."

#: ../src/common/parole-common.c:83
msgid "Message"
msgstr "Sporočilo"

#: ../src/common/parole-common.c:87
msgid "Error"
msgstr "Napaka"

#: ../src/gst/parole-gst.c:1092 ../src/parole-medialist.c:373
#, c-format
msgid "Track %i"
msgstr "Zapis %i"

#: ../src/gst/parole-gst.c:1095
msgid "Audio CD"
msgstr "Zvočni CD"

#: ../src/gst/parole-gst.c:1267
msgid "Unable to install missing codecs."
msgstr "Ne morem namestiti manjkajočih kodekov."

#: ../src/gst/parole-gst.c:1270
msgid "No available plugin installer was found."
msgstr "Program za nameščanje vtičnika ni bil najden."

#: ../src/gst/parole-gst.c:1321
msgid "Additional software is required."
msgstr "Potrebna je dodatna programska oprema."

#: ../src/gst/parole-gst.c:1325
msgid "Don't Install"
msgstr "Ne namesti"

#: ../src/gst/parole-gst.c:1327
msgid "Install"
msgstr "Namesti"

#: ../src/gst/parole-gst.c:1330
msgid "OK"
msgstr "V redu"

#: ../src/gst/parole-gst.c:1337
#, c-format
msgid ""
"Parole needs <b>%s</b> to play this file.\n"
"It can be installed automatically."
msgstr "Parole potrebuje <b>%s</b> za predvajanje te datoteke.\nMožna je samodejna namestitev."

#: ../src/gst/parole-gst.c:1340
#, c-format
msgid "Parole needs <b>%s</b> to play this file."
msgstr "Parole potrebuje <b>%s</b> za predvajanje te datoteke."

#: ../src/gst/parole-gst.c:1715
msgid "The stream is taking too much time to load"
msgstr "Za nalaganje toka je bilo potrebnega preveč časa"

#: ../src/gst/parole-gst.c:1717
msgid "Do you want to continue loading or stop?"
msgstr "Ali bi radi nadaljevali z nalaganjem ali zaustavili nalaganje?"

#: ../src/gst/parole-gst.c:1718
msgid "Stop"
msgstr "Ustavi"

#: ../src/gst/parole-gst.c:1719
msgid "Continue"
msgstr "Nadaljuj"

#: ../src/gst/parole-gst.c:1883
msgid "GStreamer Error"
msgstr "GStreamer napaka"

#: ../src/gst/parole-gst.c:1884
msgid "Parole Media Player cannot start."
msgstr "Medijskega predvajalnika Parole ni mogoče zagnati."

#: ../src/gst/parole-gst.c:1909 ../src/gst/parole-gst.c:1924
#: ../src/gst/parole-gst.c:1958
#, c-format
msgid "Unable to load \"%s\" plugin, check your GStreamer installation."
msgstr "Nalaganje vtičnika \"%s\" ni uspelo. Preverite namestitev Gstreamerja"

#: ../src/gst/parole-gst.c:2516 ../src/gst/parole-gst.c:2520
#, c-format
msgid "Audio Track #%d"
msgstr "Zvočni zapis #%d"

#: ../src/gst/parole-gst.c:2556 ../src/gst/parole-gst.c:2560
#, c-format
msgid "Subtitle #%d"
msgstr "Podnapis #%d"

#: ../src/main.c:59
#, c-format
msgid ""
"\n"
"Parole Media Player %s\n"
"\n"
"Part of the Xfce Goodies Project\n"
"https://goodies.xfce.org\n"
"\n"
"Licensed under the GNU GPL.\n"
"\n"
"Please report bugs to <https://bugzilla.xfce.org/>.\n"
"\n"
msgstr "\nParole medijski predvajalnik %s\n\nDel Xfce Goodies projekta\nhttps://goodies.xfce.org\n\nLicenca GNU GPL.\n\nHrošče prijavite na <https://bugzilla.xfce.org/>.\n\n"

#: ../src/main.c:227
msgid "Open a new instance"
msgstr "Odpri novo instanco"

#: ../src/main.c:229
msgid "Do not load plugins"
msgstr "Ne naloži vtičnikov"

#: ../src/main.c:231
msgid "Set Audio-CD/VCD/DVD device path"
msgstr "Določi pot za Audio-CD/VCD/DVD napravo"

#: ../src/main.c:233
msgid "Start in embedded mode"
msgstr "Zaženi v vsebovanem načinu"

#: ../src/main.c:235
msgid "Start in fullscreen mode"
msgstr "Zaženi v celozaslonskem načinu"

#: ../src/main.c:237
msgid "Play or pause if already playing"
msgstr "Predvajaj ali pavziraj, če se že predvaja"

#: ../src/main.c:243
msgid "Raise volume"
msgstr "Zvišaj glasnost"

#: ../src/main.c:245
msgid "Lower volume"
msgstr "Znižaj glasnost"

#: ../src/main.c:247
msgid "Mute volume"
msgstr "Utišaj glasnost"

#: ../src/main.c:249
msgid "Unmute (restore) volume"
msgstr "Od-tišaj (obnovi) glasnost"

#: ../src/main.c:251
msgid "Add files to playlist"
msgstr "Dodaj datoteke na seznam predvajanja"

#: ../src/main.c:253
msgid "Print version information and exit"
msgstr "Izpiši informacije o različici in končaj"

#: ../src/main.c:257
msgid "Media to play"
msgstr "Medij za predvajanje"

#: ../src/main.c:279
msgid "[FILES...] - Play movies and songs"
msgstr "[FILES...] - Predvajaj filme in skladbe"

#: ../src/main.c:290
#, c-format
msgid "Type %s --help to list all available command line options\n"
msgstr "Natipkajte %s --help za izpis vseh opciji ukazne vrstice\n"

#: ../src/main.c:307
#, c-format
msgid "Parole is already running, use -i to open a new instance\n"
msgstr "Parole je že zagnan, uporabite -i za novo instanco\n"

#: ../src/parole-mediachooser.c:233 ../src/parole-player.c:1003
msgid "All files"
msgstr "Vse datoteke"

#: ../src/parole-medialist.c:215 ../src/parole-medialist.c:1470
#, c-format
msgid "Playlist (%i item)"
msgid_plural "Playlist (%i items)"
msgstr[0] "Seznam predvajanja (%i predmet)"
msgstr[1] "Seznam predvajanja (%i predmeta)"
msgstr[2] "Seznam predvajanja (%i predmeti)"
msgstr[3] "Seznam predvajanja (%i predmetov)"

#: ../src/parole-medialist.c:218 ../src/parole-medialist.c:1472
#, c-format
msgid "Playlist (%i chapter)"
msgid_plural "Playlist (%i chapters)"
msgstr[0] "Seznam predvajanja (%i poglavje)"
msgstr[1] "Seznam predvajanja (%i poglavja)"
msgstr[2] "Seznam predvajanja (%i poglavja)"
msgstr[3] "Seznam predvajanja (%i poglavji)"

#: ../src/parole-medialist.c:387 ../src/parole-player.c:708
#, c-format
msgid "Chapter %i"
msgstr "Poglavje %i"

#: ../src/parole-medialist.c:648
msgid "Permission denied"
msgstr "Ni dovoljeno"

#: ../src/parole-medialist.c:650
msgid "Error saving playlist file"
msgstr "Napaka pri shranjevanju sezbnama predvajanja"

#: ../src/parole-medialist.c:660
msgid "Unknown playlist format"
msgstr "Neznan format seznama predvajanja"

#: ../src/parole-medialist.c:661
msgid "Please choose a supported playlist format"
msgstr "Izberite podprt format seznama predvajanja"

#: ../src/parole-medialist.c:704 ../src/parole-plugins-manager.c:338
msgid "Unknown"
msgstr "Neznano"

#: ../src/parole-medialist.c:788
msgid "M3U Playlists"
msgstr "M3U seznam predvajanja"

#: ../src/parole-medialist.c:796
msgid "PLS Playlists"
msgstr "PLS seznam predvajanja"

#: ../src/parole-medialist.c:804
msgid "Advanced Stream Redirector"
msgstr "Napredni preusmerjevalnik toka"

#: ../src/parole-medialist.c:812
msgid "Shareable Playlist"
msgstr "Deljiv seznam predvajanja"

#. Clear
#: ../src/parole-medialist.c:1119
msgid "Open Containing Folder"
msgstr "Odpri vsebujočo mapo"

#: ../src/parole-player.c:526
msgid "Hide Playlist"
msgstr "Skrij seznam predvajanja"

#: ../src/parole-player.c:526 ../src/parole-player.c:3397
msgid "Show Playlist"
msgstr "Pokaži seznam predvajanja"

#. Build the FileChooser dialog for subtitle selection.
#: ../src/parole-player.c:967
msgid "Select Subtitle File"
msgstr "Izberi datoteko podnapisov"

#: ../src/parole-player.c:972 ../src/parole-player.c:1159
#: ../src/parole-player.c:2811 ../src/plugins/tray/tray-provider.c:319
msgid "Cancel"
msgstr "Prekliči"

#: ../src/parole-player.c:975
msgid "Open"
msgstr "Odpri"

#: ../src/parole-player.c:991
msgid "Subtitle Files"
msgstr "Datoteke podnapisov"

#: ../src/parole-player.c:1155 ../src/parole-player.c:1163
msgid "Clear Recent Items"
msgstr "Počisti nedavne predmete"

#: ../src/parole-player.c:1157
msgid ""
"Are you sure you wish to clear your recent items history?  This cannot be "
"undone."
msgstr "Ali res želite izbrisati zgodovino predmetov? Tega ni mogoče razveljaviti."

#: ../src/parole-player.c:1260 ../src/parole-player.c:1387
msgid "Media stream is not seekable"
msgstr "Po pretok medijske vsebine se ni mogoče premikati"

#: ../src/parole-player.c:1270
msgid "Play"
msgstr "Predvajaj"

#: ../src/parole-player.c:1273
msgid "Pause"
msgstr "Pavza"

#: ../src/parole-player.c:1712
msgid "GStreamer backend error"
msgstr "Napaka na GStreamer podstati"

#: ../src/parole-player.c:1793
msgid "Unknown Song"
msgstr "Neznana pesem"

#: ../src/parole-player.c:1804 ../src/parole-player.c:1810
#: ../src/parole-player.c:1818
msgid "on"
msgstr "na"

#: ../src/parole-player.c:1818 ../src/plugins/notify/notify-provider.c:134
msgid "Unknown Album"
msgstr "Neznan album"

#: ../src/parole-player.c:1829 ../src/parole-player.c:1836
msgid "by"
msgstr "od"

#: ../src/parole-player.c:1836 ../src/plugins/notify/notify-provider.c:136
msgid "Unknown Artist"
msgstr "Neznan izvajalec"

#: ../src/parole-player.c:1871
msgid "Buffering"
msgstr "Polnjenje medpomnilnika"

#: ../src/parole-player.c:1982 ../src/parole-player.c:3372
msgid "Fullscreen"
msgstr "Cel zaslon"

#: ../src/parole-player.c:1993
msgid "Leave _Fullscreen"
msgstr "Zapusti _celozaslonski način"

#: ../src/parole-player.c:1994
msgid "Leave Fullscreen"
msgstr "Zapusti celozaslonski način"

#. Play menu item
#. * Play pause
#: ../src/parole-player.c:2096 ../src/plugins/tray/tray-provider.c:119
msgid "_Pause"
msgstr "_Predvajaj"

#: ../src/parole-player.c:2096 ../src/plugins/tray/tray-provider.c:119
msgid "_Play"
msgstr "_Zaženi"

#. * Previous item in playlist.
#: ../src/parole-player.c:2112
msgid "_Previous"
msgstr "_Prehodno"

#. * Next item in playlist.
#: ../src/parole-player.c:2123
msgid "_Next"
msgstr "_Naslednje"

#. * Un/Full screen
#: ../src/parole-player.c:2134
msgid "_Leave Fullscreen"
msgstr "_Zapusti Celozaslonski način"

#. * Un/Hide menubar
#: ../src/parole-player.c:2149
msgid "Show menubar"
msgstr "Pokaži vrstico menija"

#: ../src/parole-player.c:2166
msgid "Mini Mode"
msgstr "Mini način"

#: ../src/parole-player.c:2385
msgid "Unmute"
msgstr "Izključi nemo"

#: ../src/parole-player.c:2782
msgid "Unable to open default web browser"
msgstr "Privzetega brskalnik ni bilo mogoče odpreti"

#: ../src/parole-player.c:2784
msgid ""
"Please go to https://docs.xfce.org/apps/parole/bugs to report your bug."
msgstr "Pojdite na https://docs.xfce.org/apps/parole/bugs za prijavo hrošča."

#: ../src/parole-player.c:2812
msgid "Go"
msgstr "Pojdi"

#: ../src/parole-player.c:2825
msgid "Position:"
msgstr "Položaj"

#. Clear Recent Menu Item
#: ../src/parole-player.c:3229
msgid "_Clear recent items..."
msgstr "I_zbriši nedavne predmete"

#: ../src/parole-player.c:3548
msgid "Audio Track:"
msgstr "Zvočni zapis:"

#: ../src/parole-player.c:3567
msgid "Subtitles:"
msgstr "Podnapisi:"

#. Add a close button to the Infobar
#: ../src/parole-player.c:3573 ../src/plugins/tray/tray-provider.c:257
msgid "Close"
msgstr "Zapri"

#: ../src/parole-about.c:74
msgid "translator-credits"
msgstr "zasluge-prevajalcev"

#: ../src/parole-about.c:77
msgid "Visit Parole website"
msgstr "Obiščite spletno stran Parole"

#: ../src/parole-conf-dialog.c:287
msgid "Clutter (OpenGL)"
msgstr "Clutter (OpenGL)"

#: ../src/parole-disc.c:106
msgid "Play Disc"
msgstr "Predvajaj disk"

#: ../src/parole-subtitle-encoding.c:162
msgid "Current Locale"
msgstr "Trenutne lokalne nastavitve"

#: ../src/parole-subtitle-encoding.c:165 ../src/parole-subtitle-encoding.c:167
#: ../src/parole-subtitle-encoding.c:169 ../src/parole-subtitle-encoding.c:171
msgid "Arabic"
msgstr "Arabska"

#: ../src/parole-subtitle-encoding.c:174
msgid "Armenian"
msgstr "Armenijska"

#: ../src/parole-subtitle-encoding.c:177 ../src/parole-subtitle-encoding.c:179
#: ../src/parole-subtitle-encoding.c:181
msgid "Baltic"
msgstr "Baltska"

#: ../src/parole-subtitle-encoding.c:184
msgid "Celtic"
msgstr "Keltska"

#: ../src/parole-subtitle-encoding.c:187 ../src/parole-subtitle-encoding.c:189
#: ../src/parole-subtitle-encoding.c:191 ../src/parole-subtitle-encoding.c:193
msgid "Central European"
msgstr "Srednjeevropska"

#: ../src/parole-subtitle-encoding.c:196 ../src/parole-subtitle-encoding.c:198
#: ../src/parole-subtitle-encoding.c:200 ../src/parole-subtitle-encoding.c:202
msgid "Chinese Simplified"
msgstr "Poenostavljena kitajščina"

#: ../src/parole-subtitle-encoding.c:205 ../src/parole-subtitle-encoding.c:207
#: ../src/parole-subtitle-encoding.c:209
msgid "Chinese Traditional"
msgstr "Tradicionalna kitajščina"

#: ../src/parole-subtitle-encoding.c:212
msgid "Croatian"
msgstr "Hrvaščina"

#: ../src/parole-subtitle-encoding.c:215 ../src/parole-subtitle-encoding.c:217
#: ../src/parole-subtitle-encoding.c:219 ../src/parole-subtitle-encoding.c:221
#: ../src/parole-subtitle-encoding.c:223 ../src/parole-subtitle-encoding.c:225
msgid "Cyrillic"
msgstr "Cirilica"

#: ../src/parole-subtitle-encoding.c:228
msgid "Cyrillic/Russian"
msgstr "Cirilica/Ruska"

#: ../src/parole-subtitle-encoding.c:231 ../src/parole-subtitle-encoding.c:233
msgid "Cyrillic/Ukrainian"
msgstr "Cirilica/Ukrainska"

#: ../src/parole-subtitle-encoding.c:236
msgid "Georgian"
msgstr "Gregorjanski"

#: ../src/parole-subtitle-encoding.c:239 ../src/parole-subtitle-encoding.c:241
#: ../src/parole-subtitle-encoding.c:243
msgid "Greek"
msgstr "Grška"

#: ../src/parole-subtitle-encoding.c:246
msgid "Gujarati"
msgstr "Gujarati"

#: ../src/parole-subtitle-encoding.c:249
msgid "Gurmukhi"
msgstr "Gurmukhi"

#: ../src/parole-subtitle-encoding.c:252 ../src/parole-subtitle-encoding.c:254
#: ../src/parole-subtitle-encoding.c:256 ../src/parole-subtitle-encoding.c:258
msgid "Hebrew"
msgstr "Hebrejska"

#: ../src/parole-subtitle-encoding.c:261
msgid "Hebrew Visual"
msgstr "Hebrejsi vizualni"

#: ../src/parole-subtitle-encoding.c:264
msgid "Hindi"
msgstr "Hindujščina"

#: ../src/parole-subtitle-encoding.c:267
msgid "Icelandic"
msgstr "Islandska"

#: ../src/parole-subtitle-encoding.c:270 ../src/parole-subtitle-encoding.c:272
#: ../src/parole-subtitle-encoding.c:274
msgid "Japanese"
msgstr "Japonska"

#: ../src/parole-subtitle-encoding.c:277 ../src/parole-subtitle-encoding.c:279
#: ../src/parole-subtitle-encoding.c:281 ../src/parole-subtitle-encoding.c:283
msgid "Korean"
msgstr "Korejska"

#: ../src/parole-subtitle-encoding.c:286
msgid "Nordic"
msgstr "Nordijsko"

#: ../src/parole-subtitle-encoding.c:289
msgid "Persian"
msgstr "Perzijščina"

#: ../src/parole-subtitle-encoding.c:292 ../src/parole-subtitle-encoding.c:294
msgid "Romanian"
msgstr "Rimski"

#: ../src/parole-subtitle-encoding.c:297
msgid "South European"
msgstr "Južnoevropska"

#: ../src/parole-subtitle-encoding.c:300
msgid "Thai"
msgstr "Tajska"

#: ../src/parole-subtitle-encoding.c:303 ../src/parole-subtitle-encoding.c:305
#: ../src/parole-subtitle-encoding.c:307 ../src/parole-subtitle-encoding.c:309
msgid "Turkish"
msgstr "Turška"

#: ../src/parole-subtitle-encoding.c:312 ../src/parole-subtitle-encoding.c:314
#: ../src/parole-subtitle-encoding.c:316 ../src/parole-subtitle-encoding.c:318
#: ../src/parole-subtitle-encoding.c:320
msgid "Unicode"
msgstr "Unicode"

#: ../src/parole-subtitle-encoding.c:323 ../src/parole-subtitle-encoding.c:325
#: ../src/parole-subtitle-encoding.c:327 ../src/parole-subtitle-encoding.c:329
#: ../src/parole-subtitle-encoding.c:331
msgid "Western"
msgstr "Zahodno"

#: ../src/parole-subtitle-encoding.c:334 ../src/parole-subtitle-encoding.c:336
#: ../src/parole-subtitle-encoding.c:338
msgid "Vietnamese"
msgstr "Vietnamska"

#: ../src/parole-plugins-manager.c:253
msgid "Plugin failed to load"
msgstr "Vtičnika ni bilo mogoče naložiti"

#: ../src/parole-plugins-manager.c:254
msgid "Please check your installation"
msgstr "Preverite namestitev"

#: ../src/parole-plugins-manager.c:375
msgid "No installed plugins found on this system"
msgstr "Na tem sistemu ni najdenih vtičnikov"

#: ../src/parole-plugins-manager.c:376
msgid "Please check your installation."
msgstr "Preverite svojo namestitev."

#: ../src/misc/parole-filters.c:66
msgid "Audio"
msgstr "Zvok"

#: ../src/misc/parole-filters.c:91
msgid "Video"
msgstr "Video"

#: ../src/misc/parole-filters.c:115 ../src/misc/parole-filters.c:142
msgid "Audio and video"
msgstr "Zvok in video"

#: ../src/misc/parole-filters.c:169 ../src/misc/parole-filters.c:193
msgid "All supported files"
msgstr "Vse podprte datoteke"

#: ../src/misc/parole-filters.c:217
msgid "Playlist files"
msgstr "Datoteke seznamov predvajanj"

#: ../src/plugins/notify/notify-provider.c:139
#: ../src/plugins/notify/notify-provider.c:141
msgid "<i>on</i>"
msgstr "<i>na</i>"

#: ../src/plugins/notify/notify-provider.c:139
#: ../src/plugins/notify/notify-provider.c:141
msgid "<i>by</i>"
msgstr "<i>od</i>"

#: ../src/plugins/notify/notify.desktop.in.h:1
msgid "Notify"
msgstr "Obvesti"

#: ../src/plugins/notify/notify.desktop.in.h:2
msgid "Show notifications for currently playing tracks"
msgstr "Prikaži obvestila za trenutno predvajane skladbe"

#. * Previous Track
#: ../src/plugins/tray/tray-provider.c:128
msgid "P_revious Track"
msgstr "P_rejšnja skladba"

#. * Next Track
#: ../src/plugins/tray/tray-provider.c:137
msgid "_Next Track"
msgstr "_Naslednja skladba"

#. * Open
#: ../src/plugins/tray/tray-provider.c:153
msgid "_Open"
msgstr "_Odpri"

#: ../src/plugins/tray/tray-provider.c:254
msgid "Tray icon plugin"
msgstr "Vtičnik za ikono sistemske vrstice"

#: ../src/plugins/tray/tray-provider.c:264
msgid "Always minimize to tray when window is closed"
msgstr "Vedno zmanjšaj v sistemsko vrstico, kadar je okno zaprto"

#: ../src/plugins/tray/tray-provider.c:307
msgid "Are you sure you want to quit?"
msgstr "Ali res želite končati?"

#: ../src/plugins/tray/tray-provider.c:310
msgid "Parole can be minimized to the system tray instead."
msgstr "Parole je mogoče zmanjšati v sistemsko vrstico."

#: ../src/plugins/tray/tray-provider.c:313
msgid "Minimize to tray"
msgstr "Zmanjšaj v sistemsko vrstico"

#: ../src/plugins/tray/tray-provider.c:334
msgid "Remember my choice"
msgstr "Zapomni si mojo izbiro"

#: ../src/plugins/tray/system-tray.desktop.in.h:1
msgid "Tray icon"
msgstr "Ikona sistemske vrstice"

#: ../src/plugins/tray/system-tray.desktop.in.h:2
msgid "Show icon in the system tray"
msgstr "Pokaži ikono v sistemski vrstici"

#: ../src/plugins/mpris2/mpris2.desktop.in.h:1
msgid "MPRIS2"
msgstr "MPRIS2"

#: ../src/plugins/mpris2/mpris2.desktop.in.h:2
msgid "MPRIS2 remote control"
msgstr "MPRIS2 daljinsko upravljanje"
